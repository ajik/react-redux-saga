const initState = {
    authError: null
}

const authReducer = (state = initState, action) => {

    switch (action.type) {
        case 'RECORDS/FETCH':
            return {
                ...state,
                data: state.data
            }
        case 'RECORDS/FETCH_FAILED':
            return {
                authError: "error"
            }
        case 'RECORDS/SET':
        default:
            return state
    }
}

export default authReducer