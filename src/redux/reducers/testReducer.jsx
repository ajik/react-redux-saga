const initState = {
    data: null,
    error: null,
    fetching: true
}

function testReducer(state = initState, action) {

    switch (action.type) {
        case 'API_CALL_REQUEST':
            // console.log("reducer")
            return {
                ...state,
                data: null,
                fetching: true,
                error: null
            }
        case 'API_CALL_SUCCESS':
            // console.log(action.data)
            return {
                ...state,
                data: action.data,
                fetching: false,
                error: null
            }
        case 'API_CALL_FAILED':
            return {
                ...state,
                error: action.error,
                data: null,
                fetching: false

            }
        case 'API_TEST':
            console.log("api_test");
            return {
                ...state
            }
        default:
            return state
    }
}

export default testReducer