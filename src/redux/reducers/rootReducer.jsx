import { combineReducers } from 'redux'
import authReducer from './authReducer'
import testReducer from './testReducer'

const rootReducer = combineReducers({
    auth: authReducer,
    test: testReducer
})

export default rootReducer