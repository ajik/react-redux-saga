import testSaga from './testSagas'
import { fork, all } from 'redux-saga/effects'
import { testSaga1 } from './testSagas'

export function* rootSaga() {
    yield all([
        testSaga(),
        testSaga1()
    ])
}