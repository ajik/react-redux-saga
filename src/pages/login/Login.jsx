import React, { Component } from 'react';
import SnackBar from './SnackBar'

class Login extends Component {

    state = {
        username: '',
        password: '',
        errorUsername: '',
        errorPassword: ''
    }


    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    handleClick = (e) => {
        e.preventDefault();

        console.log(this.state.password, this.state.username)

        this.setState({
            errorUsername: 'error',
            errorPassword: 'error'
        })

        console.log("sukses");
    }

    render() {
        let errorUsername = this.state.errorUsername;
        let errorPassword = this.state.errorPassword;

        return (
            <div className="container-fluid">
                <div className="row justify-content-md-center">
                    <div className="col-md-auto">
                        <div className="card" style={{ width: '22rem', marginTop: '35%' }}>
                            <div className="card-body">
                                <h5 className="card-title">Login</h5>
                                <form>
                                    <div className="form-group">
                                        <label htmlFor="formGroupExampleInput" className="bmd-label-floating">Username</label>
                                        <input onChange={this.handleChange} type="email" className="form-control" id="username" />
                                        <small className="text-danger"> {errorUsername} </small>
                                    </div>
                                    <div className="form-group bmd-form-group">
                                        <label htmlFor="formGroupExampleInput2" className="bmd-label-floating">Password</label>
                                        <input onChange={this.handleChange} type="password" className="form-control" id="password" />
                                        <small className="text-danger"> {errorPassword} </small>
                                    </div>
                                    <div className="form-group bmd-form-group-sm">
                                        <SnackBar />
                                        <button onClick={this.handleClick} className="btn btn-primary float-right"> Button </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Login