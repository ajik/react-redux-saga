import React, { Component } from 'react';
import { connect } from 'react-redux'
import ShowData from './ShowData'

class DefaultHome extends Component {

    componentDidMount() {
        this.props.fetchData();
    }

    render() {

        const { data, addClick, fetchData, fetching, error } = this.props;

        return (
            <div>
                <button className="btn btn-primary" onClick={addClick}> button </button>
                {data == null ? error : <ShowData data={data} />}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    data: state.test.data,
    error: state.test.error,
    fetching: state.test.fetching
})


const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: () => dispatch({ type: "API_CALL_REQUEST" }),
        addClick: () => dispatch({ type: "API_TEST" })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DefaultHome)