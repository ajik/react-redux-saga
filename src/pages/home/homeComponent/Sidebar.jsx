import React from 'react';

const Sidebar = () => {
    return (
        <div>
            <header>
                <a className="navbar-brand">Title</a>
            </header>
            <ul className="list-group">
                <a className="list-group-item">Link 1</a>
                <a className="list-group-item">Link 2</a>
                <a className="list-group-item">Link 3</a>
            </ul>
        </div>
    )
}

export default Sidebar